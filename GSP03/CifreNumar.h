#pragma once


void AfisareCifre(int n);
int SumaCifre(int n);
int ProdusCifre(int n);
int CateCifre(int n);
int MaximCifre(int n);
int MinimCifre(int n);

int PrimaCifra(int n);
int InversNumar(int n);

bool Palindrom(int n); // true 13731, false pt 123
