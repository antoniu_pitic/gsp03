#include "CifreNumar.h"
#include <iostream>
using namespace std;

/*
for (int t = n ; t ; t /= 10) {
	Foloseste( t % 10 );
}
*/

void AfisareCifre(int n)
{
	for (int t = n ; t ; t /= 10) {
		cout << t % 10 << " ";
	}
	cout << endl;
}

int SumaCifre(int n)
{
	int S = 0;
	for (int t = n; t; t /= 10) {
		S += t % 10;
	}
	return S;
}

int ProdusCifre(int n)
{
	int P = 1;
	for (int t = n; t; t /= 10) {
		P *= t % 10;
	}
	return P;
}

int CateCifre(int n)
{
	int ct = 0;
	for (int t = n; t; t /= 10) {
		ct++;
	}
	return ct;
}

int MaximCifre(int n)
{
	int max = INT_MIN;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if (c > max) {
			max = c;
		}
	}
	return max;
}

int MinimCifre(int n)
{
	int min = INT_MAX;
	for (int t = n; t; t /= 10) {
		int c = t % 10;
		if (c < min) {
			min = c;
		}
	}
	return min;
}

int PrimaCifra(int n)
{
	while (n >= 10) {
		n /= 10;
	}
	return n;
}

int InversNumar(int n)
{
	int m = 0;

	for (int t = n; t; t /= 10) {
		m = m * 10 + t % 10;
	}

	return m;
}

bool Palindrom(int n)
{
	return n == InversNumar(n);
}
