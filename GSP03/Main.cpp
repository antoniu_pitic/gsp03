#include "Ecuatii.h"
#include "Serii.h"
#include "CifreNumar.h"
#include <iostream>
using namespace std;


int main() {
	SerieDeLa1La10();
	SerieDeLaAlaB(200, 205);
	SerieDeLaALaBCuPas(100, 175, 10);
	SerieDeLaALaBCuPasDescrescator(-100, 100, 50);
	PuteriAleLui2MaiMiciCa(100);
	Serie1();
	Serie2();

	cout << "----------------- cifre --------------\n";

	AfisareCifre(12345);
	cout << SumaCifre(665) << endl;
	cout << ProdusCifre(1234) << endl;
	cout << CateCifre(5643) << endl;
	cout << MinimCifre(7245) << endl;
	cout << MaximCifre(7854) << endl;
	cout << PrimaCifra(7854) << endl;
	cout << InversNumar(1234) << endl;

}