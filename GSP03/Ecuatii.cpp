#include "Ecuatii.h"
#include <iostream>
using namespace std;

void EcuatieDeGradul2()
{
	double a, b, c;
	double x1, x2;

	cout << "a="; cin >> a;
	cout << "b="; cin >> b;
	cout << "c="; cin >> c;

	double delta = b*b - 4*a*c;
	if (a != 0) {
		if (delta >= 0) {
			x1 = (-b + sqrt(delta)) / (2 * a);
			x2 = (-b - sqrt(delta)) / (2 * a);
			cout << "x1=" << x1 << endl;
			cout << "x2=" << x2 << endl;
		}
		else {
			double re, im;
			re = -b / 2 / a; // (-b)/(2*a)
			im = sqrt(-delta) / (2 * a);
			cout << "x1=" << re << "+i*" << im << endl;
			cout << "x2=" << re << "-i*" << im << endl;

		}
	}
	else {
		EcuatieDeGradul1(b, c);
	}

}

void EcuatieDeGradul1(int a, int b)
{
	
}
