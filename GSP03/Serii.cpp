#include "Serii.h"
#include <iostream>
using namespace std;

void SerieDeLa1La10()
{
	for (int i = 1; i <= 10 ; i++) {
		cout << i << " ";
	}
	cout << endl;
	// SerieDeLaAlaB(1,10);
}

void SerieDeLaAlaB(int a, int b)
{
	for (int i = a; i <= b; i++) {
		cout << i << " ";
	}
	cout << endl;
}

void SerieDeLaALaBCuPas(int a, int b, int pas)
{
	for (int i = a; i <= b; i+=pas) {
		cout << i << " ";
	}
	cout << endl;

}

void SerieDeLaALaBCuPasDescrescator(int a, int b, int pas)
{
	for (int i = b; i >= a; i -= pas) {
		cout << i << " ";
	}
	cout << endl;

}

void PuteriAleLui2MaiMiciCa(int n)
{
	for (int i = 1; i <= n; i *= 2) {
		cout << i << " ";
	}
	cout << endl;
}

// 1 9   2 8   3 7  ...   9 1
void Serie1()
{
	for (int i = 1; i <= 9; i++)
	{
		cout << i << " " << 10 - i << " ";
	}
	cout << endl;
}

// 1 9   2 8   3 7  ...   9 1
void Serie2()
{
	int i, j;

	for (i = 1, j = 9 ; i <= 9; i++, j--)
	{
		cout << i << " " << j << " ";
	}
	cout << endl;
}

